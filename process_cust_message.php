<?php
//global flags here
$prod_db = false;


// Function that takes in the name of a place and finds the coordinates of the same.
function lookup($place){

   $place = str_replace (" ", "+", urlencode($place));
   $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$place."&sensor=false";
 
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $details_url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $response = json_decode(curl_exec($ch), true);
 
   // If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
   if ($response['status'] != 'OK') {
    return null;
   }
 
   //print_r($response);
   $geometry = $response['results'][0]['geometry'];
 
    $longitude = $geometry['location']['lat'];
    $latitude = $geometry['location']['lng'];
 
    $array = array(
        'latitude' => $geometry['location']['lng'],
        'longitude' => $geometry['location']['lat'],
        'location_type' => $geometry['location_type'],
    );
 
    return $array;
}

// Function that finds the distance between two coordinates in kms.
function distance($lat1, $lon1, $lat2, $lon2) {

    $pi80 = M_PI / 180;
    $lat1 *= $pi80;
    $lon1 *= $pi80;
    $lat2 *= $pi80;
    $lon2 *= $pi80;

    $r = 6372.797; // mean radius of Earth in km
    $dlat = $lat2 - $lat1;
    $dlon = $lon2 - $lon1;
    $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $km = $r * $c;

    //echo '<br/>'.$km;
    return $km;
 }

// function to find all autos within 1 km of the customer and send them trip request message
 function finddriver($trans, $from_place, $to_place){
  $coor1 = lookup($from_place);
  $result = mysql_query("SELECT * FROM driver");
  $num = mysql_num_rows($result);
  $i=0;
  while($i<$num){
    $driver_x = mysql_result($result, $i, 'x');
    $driver_y = mysql_result($result, $i, 'y');
    $driver_phone = mysql_result($result, $i, 'phone');

    $dist = distance($coor1['latitude'], $coor1['longitude'], $driver_x, $driver_y);
    
    if($dist <= 1){
      //send message to driver
      $text = "Transaction ID: ".$trans." FROM ".$from_place." TO ".$to_place;
      $text .= ". Send SMS to 12345 as TRANS ".$trans." to accept this trip.";
      sendmessage($text,$driver_phone);
    }
    $i++;
  }
 }

// function to send SMS using MobMe's FastAlerts service.
 function sendmessage($text,$to_number){
    $xml_data ='<token>fa32910c-fd90-11e1-9429-f978630276e9</token><text>'.$text.'</text><msisdn>'.$to_number.'</msisdn><from>FALERT</from>';
    $url = "www.duo.fastalerts.in/api/sms";
  if ($GLOBALS['prod_db']) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data); // add POST fields
    $result = curl_exec($ch); // run the process
  }   
 }

// Main script begins. 

// Connect to the DB
if($prod_db){
  $con = mysql_connect("localhost","easyauto_root","admin");
  mysql_select_db("easyauto_data",$con) or die('Cant connect db'); 
} else {
  $con = mysql_connect("localhost","root","");
  mysql_select_db("easyauto",$con) or die('Cant connect db');
}

//read message by get or post. else return
if(isset($_GET['message']))
  $message=$_GET['message'];
else
  return;
//number separately
$phone = $_GET['mobileno'];

// Store all the messages received, in a database
mysql_query("INSERT INTO message(mob_number,sms) VALUES ( '".$phone."','".$message."' )");



  // Request from the customer for a trip
  // format - origin * destination 
  // convert user position to x-y coordinates

  $city = "trivandrum";
  $split = explode('*', $message);
  $from_place = $split[0];
  $from_place = $from_place.", ".$city;

  $to_place = $split[1];
  $to_place = $to_place.", ".$city; 
  
  $text = "Hello! Thanks for using EasyAuto. Please give us a few moments while we find an auto for you.";
  sendmessage($text,$phone);

  $result = mysql_query("SELECT max(trans) from customer");
  $trans = mysql_result($result, 0);
  if($trans == null)
    $trans = 1;
  else
    $trans++;
  mysql_query("INSERT INTO customer(trans,phone,status) VALUES ( 
        '".$trans."',
        '".$phone."',
        '0'
         ) "); 

   finddriver($trans,$from_place,$to_place);

?>